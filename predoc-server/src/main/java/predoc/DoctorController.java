package predoc;

import predoc.core.OtherListDto;
import predoc.core.SymptomeDto;
import predoc.core.SyndromeDto;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by olivier on 19/03/16.
 */
@ManagedBean
@ViewScoped
public class DoctorController implements Serializable {
    private List<String> answerAlert;
    private Double notUnderstandAnswer;
    private Double notAnswereScore;
    private Double notKnowAnswerScore;
    /** **/
    private OtherListDto autres;
    private SymptomeDto symptomePrincipal;

    public DoctorController() {

        /** Symptome principale */
        symptomePrincipal = new SymptomeDto();
        symptomePrincipal.setLabel("Douleur Abdominale");
        symptomePrincipal.setSymptomeDetails(Arrays.asList("Periombilicale","Modérée","Depuis 3 jours","Aggravation progressive"));

        /** autres */
        autres = new OtherListDto();
        /** syndrome polyuro*/
        SymptomeDto polyUri = new SymptomeDto();
        polyUri.setLabel("Polyurie");
        SymptomeDto polyDypsie = new SymptomeDto();
        polyDypsie.setLabel("Polydypsie");
        List<SymptomeDto> polyUroList = Arrays.asList(polyUri,polyDypsie);
        SyndromeDto polyUro = new SyndromeDto();
        polyUro.setLabel("Syndrôme Polyuropolydypsique");
        polyUro.setSymptoneDtoList(polyUroList);

        /** syndrome cardinal*/
        SyndromeDto cardinal = new SyndromeDto();
        cardinal.setLabel("Syndrôme Cardinal");
        cardinal.setSyndromeDtoList(Arrays.asList(polyUro));
        SymptomeDto amaigrissement = new SymptomeDto();
        amaigrissement.setLabel("Amaigrissement");
        cardinal.setSymptoneDtoList(Arrays.asList(amaigrissement));
        autres.setSyndromeDtoList(new ArrayList<>(Arrays.asList(cardinal)));

        SymptomeDto nausees = new SymptomeDto();
        nausees.setLabel("Nausées");
        SymptomeDto vomissements = new SymptomeDto();
        vomissements.setLabel("Vomissements");
        autres.setSymptoneDtoList(new ArrayList<>(Arrays.asList(nausees,vomissements)));

        /** fiabilité */
        notUnderstandAnswer = 1.3;
        notKnowAnswerScore = 1.3;
        notAnswereScore = 98.7;

        answerAlert = new ArrayList<>();
        /** Alertes */
        answerAlert.add("Voyage au Ghana");
        answerAlert.add("Allergies aux médicaments");

    }

    public List<String> getAnswerAlert() {
        return answerAlert;
    }

    public void setAnswerAlert(List<String> answerAlert) {
        this.answerAlert = answerAlert;
    }

    public Double getNotUnderstandAnswer() {
        return notUnderstandAnswer;
    }

    public void setNotUnderstandAnswer(Double notUnderstandAnswer) {
        this.notUnderstandAnswer = notUnderstandAnswer;
    }

    public Double getNotAnswereScore() {
        return notAnswereScore;
    }

    public void setNotAnswereScore(Double notAnswereScore) {
        this.notAnswereScore = notAnswereScore;
    }

    public Double getNotKnowAnswerScore() {
        return notKnowAnswerScore;
    }

    public void setNotKnowAnswerScore(Double notKnowAnswerScore) {
        this.notKnowAnswerScore = notKnowAnswerScore;
    }

    public SymptomeDto getSymptomePrincipal() {
        return symptomePrincipal;
    }

    public void setSymptomePrincipal(SymptomeDto symptomePrincipal) {
        this.symptomePrincipal = symptomePrincipal;
    }

    public OtherListDto getAutres() {
        return autres;
    }

    public void setAutres(OtherListDto autres) {
        this.autres = autres;
    }
}
