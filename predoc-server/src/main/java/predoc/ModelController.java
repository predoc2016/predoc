package predoc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import predoc.model.Concept;
import sun.misc.IOUtils;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Quentin on 19/03/2016.
 */
@ManagedBean(eager=true)
@ApplicationScoped
public class ModelController implements Serializable {

    private List<Concept> concepts;


    public ModelController() {
        loadData();
    }

    private void loadData() {
        Gson gson = new GsonBuilder().create();

        try{
            InputStream inputStream = FacesContext.getCurrentInstance().getExternalContext()
                    .getResourceAsStream("/resources/json/concepts.json");

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            reader.close();


            String json = out.toString();

            concepts = gson.fromJson(json, new TypeToken<List<Concept>>(){}.getType());

        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public List<Concept> getConcepts() {
        return concepts;
    }

    public void setConcepts(List<Concept> concepts) {
        this.concepts = concepts;
    }
}
