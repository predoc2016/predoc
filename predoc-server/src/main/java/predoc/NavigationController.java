package predoc;

import org.primefaces.context.RequestContext;
import predoc.core.DataBase;
import predoc.core.FormulationDto;
import predoc.model.*;
import predoc.model.Choice;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.awt.*;
import java.io.Serializable;
import java.util.*;
import java.util.List;

/**
 * Created by olivier on 19/03/16.
 */
@ManagedBean
@ViewScoped
public class NavigationController implements Serializable {

    private static final long serialVersionUID = 1590619060280436184L;

    @ManagedProperty("#{modelController}")
    private ModelController modelController;
    @ManagedProperty("#{dataBase}")
    private DataBase dataBaseDto;

    private Random rand = new Random();

    private List<FormulationDto> formulationList = new ArrayList<FormulationDto>();

    private int formulationIndex = 0;

    private Node currentNode;
    private List<Node> allNode;

    @PostConstruct
    public void init() {
        allNode = new ArrayList<>();

        if(modelController.getConcepts().size()>0) {
            List<Question> questions = modelController.getConcepts().get(0).getQuestions();
            if(questions != null) {
                for(Question question : questions) {
                    if(question.getFirst() != null && question.getFirst()) {
                        currentNode = generateNode(question.getIdentifier());
                        break;
                    }
                }
            }
        }

        formulationList.add(generateFormulation());
    }

    public Node generateNode(String questionId) {
        Question question = getQuestionById(questionId);

        Node node = new Node();
        node.setQuestionId(questionId);
        node.setChoices(question.getChoices());
        List<Node> nodeChildren = new ArrayList<>();

        if(question.getChoices() != null) {
            for (Choice choice : question.getChoices()) {
                if (choice.getNextQuestionId() != null) {
                    Node child = getNode(choice.getNextQuestionId());
                    if(child == null) {
                        child = generateNode(choice.getNextQuestionId());
                    }
                    if(!hasNode(nodeChildren,child.getQuestionId())) {
                        nodeChildren.add(child);
                    }
                }
            }
        }

        node.setChildrens(nodeChildren);

        allNode.add(node);
        return node;
    }

    public Boolean hasNode(List<Node> list, String questionId) {
        Boolean result = Boolean.FALSE;

        for(Node node : list) {
            if(node.getQuestionId().equals(questionId)) {
                result = Boolean.TRUE;
            }
        }

        return result;
    }

    public Node getNode(String questionId) {
        if(allNode != null) {
            for(Node node : allNode) {
                if(node.getQuestionId().equals(questionId)) {
                    return node;
                }
            }
        }
        return null;
    }

    public Question getQuestionById(String questionId) {
        List<Concept> concepts = modelController.getConcepts();
        Concept concept = concepts.get(0);

        for(Question question : concept.getQuestions()) {
            if(question.getIdentifier().equals(questionId)) {
                return question;
            }
        }
        return null;
    }

    private Boolean notLast(FormulationDto dto) {
        Boolean result = Boolean.FALSE;
        for(int i = 0; i < formulationList.size()-1; i++) {
            if(formulationList.get(i).getIdQuestion().equals(dto.getIdQuestion())) {
                result = Boolean.TRUE;
            }
        }
        return result;
    }
    public String next(FormulationDto dto) {
        formulationIndex = 0;
        Boolean hasNext = Boolean.FALSE;
        if(notLast(dto)) {
            Iterator<FormulationDto> iterator = formulationList.iterator();
            Boolean remove = Boolean.FALSE;
            while(iterator.hasNext()) {
                FormulationDto currentDto = iterator.next();
                if(remove) {
                    iterator.remove();
                }
                if(currentDto.getIdQuestion().equals(dto.getIdQuestion())) {
                    remove = Boolean.TRUE;
                }
            }
            for(Node node: allNode) {
                if(node.getQuestionId().equals(dto.getIdQuestion())) {
                    currentNode = node;
                    break;
                }
            }
        }

        if (currentNode != null) {
            currentNode = currentNode.getNextNode(dto);
            if (currentNode != null) {
                hasNext = Boolean.TRUE;
            }

            formulationList.get(formulationList.size() - 1).setFirst(Boolean.TRUE);
            formulationList.add(generateFormulation());

        }

        if(!hasNext) {
            RequestContext.getCurrentInstance().execute("");
        } else if (formulationList.get(formulationList.size() - 1).getType() == 102) {
            String script = "Materialize.fadeInImage('.card:last-child');LoadCorps();scrollToBottom(); ";
            RequestContext.getCurrentInstance().execute(script);
        }
        return "page1";
    }

    public void newFormulation() {
        formulationIndex++;
        formulationList.set(formulationList.size() - 1, generateFormulation());
        if(formulationList.get(formulationList.size()-1).getType() == 102) {
            String script = "Materialize.fadeInImage('.card:last-child');LoadCorps()";
            RequestContext.getCurrentInstance().execute(script);
        }
    }

    public void done() {
        dataBaseDto.setFormulationDtoList(formulationList);
    }

    private FormulationDto generateFormulation() {
        FormulationDto formulationDto = new FormulationDto();
        if(formulationList.isEmpty()) {
            formulationDto.setFirst(Boolean.TRUE);
        }

        List<Concept> concepts = modelController.getConcepts();
        Concept concept = concepts.get(0);

        Question nextQuestion = null;

        String questionId = currentNode.getQuestionId();

        for(Question question : concept.getQuestions()) {
            if(question.getIdentifier().equals(questionId)) {
                nextQuestion = question;
                formulationDto.setIdQuestion(question.getIdentifier());
            }
        }

        if(nextQuestion != null) {
            List<Formulation> formulations = nextQuestion.getFormulations();
            Formulation formulation = formulations.get(formulationIndex % formulations.size());
            int type = nextQuestion.getType().getValue() * 100 + formulation.getType().getValue();
            formulationDto.setType(type);
            formulationDto.setIdFormulation(formulation.getIdentifier());
            formulationDto.setIdConcept(concept.getIdentifier());
            formulationDto.setLabel(formulation.getQuestion());

            switch (nextQuestion.getType()) {
                case SELECTONE:
                case SELECTMANY:
                    List<Object> choiceLabels = new ArrayList<>();
                    for(Choice choice : nextQuestion.getChoices()) {
                        choiceLabels.add(choice.getLabel());
                    }
                    formulationDto.setAnswerList(choiceLabels);
                    break;
            }
        }

        return formulationDto;
    }

    public Node getCurrentNode() {
        return currentNode;
    }

    public void setCurrentNode(Node currentNode) {
        this.currentNode = currentNode;
    }

    public List<FormulationDto> getFormulationList() {
        return formulationList;
    }

    public void setFormulationList(List<FormulationDto> formulationList) {
        this.formulationList = formulationList;
    }

    public ModelController getModelController() {
        return modelController;
    }

    public void setModelController(ModelController modelController) {
        this.modelController = modelController;
    }

    public DataBase getDataBaseDto() {
        return dataBaseDto;
    }

    public void setDataBaseDto(DataBase dataBaseDto) {
        this.dataBaseDto = dataBaseDto;
    }

    public int getFormulationIndex() {
        return formulationIndex;
    }

    public void setFormulationIndex(int formulationIndex) {
        this.formulationIndex = formulationIndex;
    }
}
