package predoc;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

/**
 * Created by Quentin on 19/03/2016.
 */
@ManagedBean
@SessionScoped
public class UtilBean {

    public Object getMin(List<Object> list) {
        return 35;
    }

    public Object getMax(List<Object> list) {
        return 39;
    }
}
