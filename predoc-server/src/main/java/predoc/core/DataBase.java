package predoc.core;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Jerome on 19/03/2016.
 */

@ManagedBean(eager=true)
@ApplicationScoped
public class DataBase implements Serializable{

    private List<FormulationDto> formulationDtoList;

    public List<FormulationDto> getFormulationDtoList() {
        return formulationDtoList;
    }

    public void setFormulationDtoList(List<FormulationDto> formulationDtoList) {
        this.formulationDtoList = formulationDtoList;
    }
}
