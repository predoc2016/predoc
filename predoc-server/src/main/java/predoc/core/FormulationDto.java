package predoc.core;

import java.io.Serializable;
import java.util.List;

/**
 * Created by olivier on 19/03/16.
 */
public class FormulationDto implements Serializable {
    private String label;
    private Integer type;
    private String idFormulation;
    private String idQuestion;
    private String idConcept;
    private List<Object> answerList;
    private Object oneAnswer;
    private List<Object> multipleAnswers;
    private Boolean first = Boolean.FALSE;

    public String getIdFormulation() {
        return idFormulation;
    }

    public void setIdFormulation(String idFormulation) {
        this.idFormulation = idFormulation;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIdConcept() {
        return idConcept;
    }

    public void setIdConcept(String idConcept) {
        this.idConcept = idConcept;
    }

    public List<Object> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Object> answerList) {
        this.answerList = answerList;
    }

    public Object getOneAnswer() {
        return oneAnswer;
    }

    public void setOneAnswer(Object oneAnswer) {
        this.oneAnswer = oneAnswer;
    }

    public List<Object> getMultipleAnswers() {
        return multipleAnswers;
    }

    public void setMultipleAnswers(List<Object> multipleAnswers) {
        this.multipleAnswers = multipleAnswers;
    }

    public Boolean getFirst() {
        return first;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public String getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }
}
