package predoc.core;

import java.util.List;

/**
 * Created by Jerome on 20/03/2016.
 */
public class OtherListDto {

    private List<SymptomeDto> symptomeDtoList;
    private List<SyndromeDto> syndromeDtoList;

    public List<SyndromeDto> getSyndromeDtoList() {
        return syndromeDtoList;
    }

    public void setSyndromeDtoList(List<SyndromeDto> syndromeDtoList) {
        this.syndromeDtoList = syndromeDtoList;
    }

    public List<SymptomeDto> getSymptoneDtoList() {
        return symptomeDtoList;
    }

    public void setSymptoneDtoList(List<SymptomeDto> symptomeDtoList) {
        this.symptomeDtoList = symptomeDtoList;
    }
}
