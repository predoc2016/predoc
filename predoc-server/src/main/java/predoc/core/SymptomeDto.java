package predoc.core;

import java.io.Serializable;
import java.util.List;

/**
 * Created by olivier on 19/03/16.
 */
public class SymptomeDto implements Serializable {
    private String label;
    private List<QuestionDto> causeDtoList;
    private double trigger;
    private List<String> symptomeDetails;


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<QuestionDto> getCauseDtoList() {
        return causeDtoList;
    }

    public void setCauseDtoList(List<QuestionDto> causeDtoList) {
        this.causeDtoList = causeDtoList;
    }

    public double getTrigger() {
        return trigger;
    }

    public void setTrigger(double trigger) {
        this.trigger = trigger;
    }

    public List<String> getSymptomeDetails() {
        return symptomeDetails;
    }

    public void setSymptomeDetails(List<String> symptomeDetails) {
        this.symptomeDetails = symptomeDetails;
    }

    @Override
    public String toString() {
        return label;
    }


}
