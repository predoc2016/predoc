package predoc.core;

import java.io.Serializable;
import java.util.List;

/**
 * Created by olivier on 19/03/16.
 */
public class SyndromeDto implements Serializable {
    private List<SymptomeDto> symptoneDtoList;

    private List<SyndromeDto> syndromeDtoList;

    private String label;
    private double trigger;

    public List<SymptomeDto> getSymptoneDtoList() {
        return symptoneDtoList;
    }

    public void setSymptoneDtoList(List<SymptomeDto> symptoneDtoList) {
        this.symptoneDtoList = symptoneDtoList;
    }

    public List<SyndromeDto> getSyndromeDtoList() {
        return syndromeDtoList;
    }

    public void setSyndromeDtoList(List<SyndromeDto> syndromeDtoList) {
        this.syndromeDtoList = syndromeDtoList;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getTrigger() {
        return trigger;
    }

    public void setTrigger(double trigger) {
        this.trigger = trigger;
    }

    @Override
    public String toString() {
        return label;
    }
}
