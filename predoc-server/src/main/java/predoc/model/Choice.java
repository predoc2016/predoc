package predoc.model;

/**
 * Created by Quentin on 20/03/2016.
 */
public class Choice {

    private String label;

    private String nextQuestionId;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNextQuestionId() {
        return nextQuestionId;
    }

    public void setNextQuestionId(String nextQuestionId) {
        this.nextQuestionId = nextQuestionId;
    }
}
