package predoc.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Quentin on 19/03/2016.
 */
public class Concept implements Serializable {

    private static final long serialVersionUID = 7774645342058082417L;

    private String identifier;

    private String label;

    private List<Question> questions;

    public Concept() {
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
