package predoc.model;

import java.io.Serializable;

/**
 * Created by Quentin on 19/03/2016.
 */
public class Formulation implements Serializable {

    private static final long serialVersionUID = 7546564059150913348L;

    public enum FormulationType {
        TEXT(0), IMAGE(1), SVG(2), SLIDER(3);

        private int value;

        FormulationType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    private String identifier;

    private String question;

    private FormulationType type;

    public Formulation() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public FormulationType getType() {
        return type;
    }

    public void setType(FormulationType type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
