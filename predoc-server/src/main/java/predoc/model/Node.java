package predoc.model;

import predoc.core.FormulationDto;

import java.awt.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Quentin on 20/03/2016.
 */
public class Node implements Serializable {

    private String questionId;

    private List<Node> childrens;

    private List<Choice> choices;

    public Node getNextNode(FormulationDto dto) {
        if(dto.getIdQuestion().equals(questionId)) {
            if(childrens == null || childrens.size()==0) {
                return null;
            } else if(childrens.size()==1) {
                return childrens.get(0);
            }
            else {
                if(choices != null) {
                    for (Choice choice : choices) {
                        if (choice.getLabel().equals(dto.getOneAnswer())) {
                            return getChildren(choice.getNextQuestionId());
                        }
                    }
                }
            }
        }
        else {
            for(Node node : childrens) {
                Node n = node.getNextNode(dto);
                if(n != null) {
                    return n;
                }
            }
        }
        return null;
    }

    private Node getChildren(String questionId) {
        for(Node node : childrens) {
            if(node.getQuestionId().equals(questionId)) {
                return node;
            }
        }
        return null;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<Node> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<Node> childrens) {
        this.childrens = childrens;
    }
}
