package predoc.model;

import java.awt.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Jerome on 19/03/2016.
 */
public class Question implements Serializable {

    private static final long serialVersionUID = -9035299316201881243L;

    public enum QuestionType{
        SELECTONE(0), SELECTMANY(1);

        private int value;

        QuestionType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    private String identifier;

    private QuestionType type;

    private List<Choice> choices;

    private List<Formulation> formulations;

    private Boolean first;

    public Question() {
    }

    public Boolean getFirst() {
        return first;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public List<Formulation> getFormulations() {
        return formulations;
    }

    public void setFormulations(List<Formulation> formulations) {
        this.formulations = formulations;
    }



}
